<?php
class PDO_Actions {

    private $DBcon;
    //protected
    public function __construct(PDO $DBcon) {
       $this->DBcon = $DBcon;
    }

    //////////
    function insert_2_db($t_table, $t_records)
    {
        $count = 0;
        $yet_another_array = array();

        foreach ($t_records as $key => $val)
        {
            if ($count === 0) {
              $t_db_fields = $key;
              $t_db_fields_with_colon = ':'.$key;
              if ($val === '') $t_values = "''"; else $t_values = "'" . $val . "'";
            }
            else {
              $t_db_fields .= ", " . $key;
              $t_db_fields_with_colon .= ', ' . ':'.$key;
              if ($val === '') $t_values .= ", ''"; else $t_values .= ", '" . $val . "'";
            }
        $count++;
        $yet_another_array[':'.$key] = $val;
        }

        $sql = "INSERT INTO " . $t_table . " (" . $t_db_fields . ") VALUES (" . $t_db_fields_with_colon . ")";
        $pdo_statement = $this->DBcon->prepare( $sql );
        $pdo_statement->execute( $yet_another_array );
    }

    //////////
    function print_from_db($co, $t_table, $t_condition='', $ekstra='')
    {
        $q = $this->select_from_db($co, $t_table, $t_condition, $ekstra);
        foreach($q as $row) {
            return $row[$co];
        }
    }

    //////////
    function select_from_db($co, $t_table, $t_condition='', $ekstra='')
    {
        $t_condition = ($t_condition !== '') ? ' WHERE ' . $t_condition : '';
        $ekstra = ($ekstra !== '') ? ' ' . $ekstra : '';

        $pdo_statement = $this->DBcon->prepare("SELECT " . $co . " FROM " . $t_table . $t_condition . $ekstra);
        $pdo_statement->execute();
        return $pdo_statement->fetchAll();
    }


    //////////
    function update_db($t_table, $t_records, $t_condition)
    {
        $count = 0;
        foreach ($t_records as $key => $val)
        {
            if ($count === 0) {
                $t_db_fields = $key . " = '" . $val . "'";
            }
            else {
                $t_db_fields .= ", " . $key . " = '" . $val . "'";
            }

            $count++;
        }
        $pdo_statement = $this->DBcon->prepare("UPDATE " . $t_table . " SET " . $t_db_fields . " WHERE " . $t_condition . "");
        $pdo_statement->execute();
    }

    //////////
    function delete_from_db($t_table, $t_condition)
    {
      $pdo_statement = $this->DBcon->prepare("DELETE from " . $t_table . " WHERE " . $t_condition . "");
      $pdo_statement->execute();
    }
}