First, you need to initiate the DB connection:

	<?php
	try
	{
		$DBcon = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
		$DBcon->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e)
	{
		echo 'ERROR : '.$e->getMessage();
		die('Not good... not good..');
	}
	?>

Somewhere in your code you need to activate the class:

	<?php
		$action = new PDO_Actions($DBcon);
	?>

##Example usage:

You use it exactly same as the [ other function listed here ](https://bitbucket.org/gregorybialowas/crud-operations/src/master/)

###Select from db
	<?php
		$q = $action->select_from_db('*', 'table', 'column = ' . $variable );
	?>

	<?php	
		$q = $action->select_from_db('*', TABLE, '', 'ORDER BY order_this');
	?>
	
###Insert to db	
	<?php		
	    $insert_array = array(
			'name' => $_POST['name'],
		);
		$action->insert_2_db(TABLE, $insert_array);
	?>
		
###Edit db
	<?php		
		$insert_array = array(
			'name' => $_POST['name'],
		);
		$action->update_db(TABLE, $insert_array, 'id = \'' . (int)$id . '\'');	
	?>

###Delete from db	
	<?php		
		$action->delete_from_db(TABLE, 'id = \'' . (int)$id . '\'');
	?>	
	
	
##Example init from my site, from the config.php:

	<?php
	require_once( 'db.conn.php' );
	require_once( 'crud-pdo.php' );

	$action = new PDO_Actions($DBcon);
	?>
	
##[ For more info go here ](http://gregbialowas.com/pdo-operations)	